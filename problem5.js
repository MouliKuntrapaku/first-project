function problem5(array)
{
    let count=0;
    for(let index=0; index<array.length; index++)
    {
        if(array[index]<2000)
        {
            count+=1;
        }
    }
    return count;
}

module.exports = problem5;