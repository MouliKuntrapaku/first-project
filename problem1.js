function problem1(inventory)
{
    for(let index=0;index<inventory.length;index++)
    {
        const car = inventory[index];
        if(car.id==33)
        {
            return car;
        }
    }
    return [];
}
module.exports = problem1;