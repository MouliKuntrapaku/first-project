function problem6(inventory)
{
    let array=[];
    for(let index=0; index<inventory.length; index++)
    {
        if(inventory[index].car_make=="BMW" || inventory[index].car_make=="Audi")
        {
            array.push(inventory[index]);
        }
    }
    return array;
}

module.exports = problem6;