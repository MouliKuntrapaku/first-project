function problem3(inventory)
{
    let arr=[];
    for(let index=0;index<inventory.length;index++)
    {
        arr.push(inventory[index].car_model);
    }
    if(arr.length!=0)
    {
        return arr.sort();
    }
    else
    {
        return [];
    }
}

module.exports = problem3;